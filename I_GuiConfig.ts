export interface I_utConfig {
    lessThanShortLegsCM: number;
    greaterThanLongLegsCM: number;

    lessThanAverageBMI: number;
    greaterThanAverageBMI: number;

    imagesForStyles: string[];
}


export interface I_GuiConfig {
    userTypes: string[];
    retailerName: string;

    $fontFamily: string;
    $fontUrl: string;

    displayLang: string;
    textDirection: string;
    currencyCode: string;

    cdnBaseUrl: string;
    users: I_utConfig[];
}
