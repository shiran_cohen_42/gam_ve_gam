var fs = require('fs');

export interface I_System {
    production: boolean;
    retailersList: string[];
    updaterIndex: number;
    dbProductionStr: string;
    dbLocalStr: string;
    getXmlFromWeb: boolean;
    importToDb: boolean;

    Regions: string[];
    Units: string[];
    numItemsPerPage: number;
}

export interface UsersData {
    exelFileName: string;
    baseAlg: string;
    featue: string;
    category: string;
    saveDictionary: boolean[];
    algs: string[][];
}

export interface I_UserTypeData {
    usersData: UsersData[];
}

export class C_System {
    mBaseDir = '..//data_store//';
    mRetailersDir = 'retailers//';
    mUserTypeDir = 'userTypes//';
    mLang = 'lang//';
    mLangPath = '';
    mRetailerPath = '';
    mData: I_System;
    mUserTypeData: I_UserTypeData;

    constructor() {
        this.mRetailerPath = this.mBaseDir + this.mRetailersDir;
        this.mLangPath = this.mBaseDir + this.mLang;
        this.mData = this.init();
        this.mUserTypeData = this.getUTypeData();
        this.verifications();
    }

    get(): I_System {
        return this.mData;
    }

    getUserTypeData(): I_UserTypeData {
        return this.mUserTypeData;
    }
    getRetailerPath(rName: string): string {
        return this.mRetailerPath + '//' + rName + '//';
    }



    private init(): I_System {
        const fullPath = this.mBaseDir + 'system.json';
        const data = this.readFile(fullPath);
        return data as I_System;
    }

    private getUTypeData(): I_UserTypeData {
        const fullPath = this.mBaseDir + this.mUserTypeDir + 'base_algs.json';
        const data = this.readFile(fullPath);
        return data as I_UserTypeData;
    }


    private verifyRetailer(name: string): boolean {
        // firts look at the list
        let found = false;
        this.mData.retailersList.forEach(retailerName => {
            if (retailerName === name) { found = true; }
        });
        console.assert(found);
        return found;
    }


    private verifications(): void {
        this.mData.retailersList.forEach(retailerName => {
            const path = this.mRetailerPath + retailerName;
            console.assert(fs.existsSync(path));
            this.verifyRetailer(retailerName);
        });
    }


    private readFile(fullPath: string): any {
        console.assert(fs.existsSync(fullPath));
        const rawdata = fs.readFileSync(fullPath);
        return JSON.parse(rawdata);
    }

}