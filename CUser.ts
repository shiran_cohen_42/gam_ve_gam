import { CAbsCategory } from "./C_AbsCategory";
import { I_GuiConfig } from './I_GuiConfig';
export interface I_UTRoptions {
  userTypes: string[];
  retailers: string[];
}

export interface I_UTR {
  userType: string;
  retailer: string;
}

export class CUser {
  private mRetailer: I_GuiConfig;
  private mFullOptions: CAbsCategory;
  private mUser: CAbsCategory;
  private mUT: string;

  constructor(workVals: CAbsCategory, fullOptions: CAbsCategory, config: I_GuiConfig) {
    this.mUser = workVals;
    this.mFullOptions = fullOptions;
    this.mRetailer = config;
    this.mUT = this.getUT();
  }

  getUT(): string {
    let rv = '';
    const f = this.mUser.getFeature('UserType');
    if (f !== undefined) {
      rv = f.getCurValue();
    } else {
      console.assert(false);
    }
    return rv;
  }

  getUTR(): I_UTR {
    return { userType: this.mUT, retailer: this.mRetailer.retailerName } as I_UTR;
  }

  getTheFullName(): string {
    const utr = this.getUTR();
    return utr.userType + '_' + utr.retailer;
  }

  getUser(): CAbsCategory {
    console.assert(this.mUser !== undefined);
    return this.mUser;
  }

  getUserCopy(): CAbsCategory {
    console.assert(this.mUser !== undefined);
    return this.mUser.copyConstructor();
  }

  getOptions(): CAbsCategory {
    console.assert(this.mFullOptions !== undefined);
    return this.mFullOptions;
  }

  getOptionsCopy(): CAbsCategory {
    console.assert(this.mFullOptions !== undefined);
    return this.mFullOptions.copyConstructor();
  }

  getRetailerName(): string {
    return this.mRetailer.retailerName;
  }

  isReady(): boolean {
    this.setIfPossible();
    return this.mUser.readyForWork();
  }

  updateRetailer(retailer: I_GuiConfig) {
    console.assert(retailer !== undefined);
    this.mRetailer = retailer;
  }

  update(by: CAbsCategory): void {
    this.mUser.vals = by.vals;
  }

  updateFeature( feature: string, val: string[]) {
    this.mUser.setCategory(feature, val);
  }


  // Micah - you have done it again... 'Thin' is a tagVal... need to find
  // a way to connect such functions to dict
  // Since this is retailer sensitive we pass in the thresholds
  setBodySize(lessThanAverage: number, greaterThanAverage: number) {
    const feature = 'BodySize';
    const heightInCm = this.mUser.getVal('Height');
    const weightInKg = this.mUser.getVal('Weight');
    const bmi = Number(weightInKg) / (Math.pow(Number(heightInCm) / 100, 2));

    const ff = this.mFullOptions.getFeature(feature);
    const fToUpdate = this.mUser.getFeature(feature);
    if (ff !== undefined && fToUpdate !== undefined) {
      console.assert(ff.vals.length === 3, 'Full feature of BodyZise must be 3 for below calc');
      if (bmi < lessThanAverage) {
        fToUpdate.setVal(ff.vals[0], false);
      } else if (bmi <= greaterThanAverage) {
        fToUpdate.setVal(ff.vals[1], false);
      } else {
        fToUpdate.setVal(ff.vals[2], false);
      }
    }
  }

  setLegSize(lessThanShortLegsCM: number, greaterThanLongLegsCM: number) {
    // Micah does it again
    if (this.getUT() !== 'male') {
      return;
    }
    const feature = 'LegLength';

    const ll = this.mFullOptions.getFeature(feature);
    if (ll === undefined) {
      console.assert(false);
    } else {
      console.assert(ll.vals.length === 3, 'Full feature of BodyZise must be 3 for below calc');

      const heightInCm = Number(this.mUser.getVal('Height'));
      const fToUpdate = this.mUser.getFeature(feature);
      if (fToUpdate !== undefined) {
        if (heightInCm < lessThanShortLegsCM) {
          fToUpdate.setVal(ll.vals[0], false);
        } else if (heightInCm <= greaterThanLongLegsCM) {
          fToUpdate.setVal(ll.vals[1], false);
        } else {
          fToUpdate.setVal(ll.vals[2], false);
        }
      }

    }
  }


  getUti(ut: string): number {
    let rv = -1;
    this.mRetailer.userTypes.forEach((user, i) => {
      if (user === ut) {
        rv = i;
      }
    })

    console.assert(rv > -1);
    return rv;

  }

  setIfPossible() {
    const h = this.mUser.getVal('Height');
    if (h === undefined || h.length === 0) return;
    const w = this.mUser.getVal('Weight');
    if (w === undefined || w.length === 0) return;

    const uti = this.getUti(this.getUT());
    this.setBodySize(this.mRetailer.users[uti].lessThanAverageBMI,
      this.mRetailer.users[uti].greaterThanAverageBMI);

    this.setLegSize(this.mRetailer.users[uti].lessThanShortLegsCM,
      this.mRetailer.users[uti].greaterThanLongLegsCM);
  }

}
