// import * as XLSX from 'xlsx';
var XLSX = require('xlsx');

import { Workbook, C_ExelData } from './C_ExelData';

export class CExelFile {
    data: Workbook[] = [];
    ready = false;
    fileName = '';
    constructor(fileName: string) {
        const wb = XLSX.readFile(fileName, { type: 'binary' });

        /* grab all workbooks */
        wb.SheetNames.forEach((wbName: string) => {
            const ws = wb.Sheets[wbName];
            let tdata: Array<Array<any>>;

            /* save data */
            tdata = XLSX.utils.sheet_to_json(ws, { header: 1 });
            const w = new Workbook(wbName, tdata);
            this.data.push(w);
        });

    }

    getJustThedata(): C_ExelData {
        const aFile = new C_ExelData(this.data);
        return aFile;
    }
}
