import { C_ExelData } from './C_ExelData';
import { C_Catalog } from './C_Catalog';
import { C_System, UsersData } from './C_System';
import { CExelFile } from './CExel';
import { I_BaseUserAlgs } from './I_BaseUserAlgs';
var file = require('fs');

export class C_UserBuilder {
  mBaseAlg!: string;
  outPath = '..//gam_ve_gam//'
  thePath = '..//data_store//';
  mCatalogs: C_Catalog[] = [];

  constructor() {
    const sys = new C_System();
    const usersData = sys.getUserTypeData().usersData;
    for (let i = 0; i < usersData.length; i++) {
      const uTinfo = new I_BaseUserAlgs(usersData[i]);
      const fullPath = this.thePath + 'userTypes//' + uTinfo.UT + '//' + uTinfo.exelFileName;
      const exelFile = new CExelFile(fullPath).getJustThedata();

      this.buildDictionary(uTinfo, exelFile);
    }
  }

  getCatalogs(): C_Catalog[] {
    return this.mCatalogs;
  }

  getOutPath(dictionary: boolean): string {
    if (dictionary) {
      return this.outPath + this.mBaseAlg + '_dictionary.json';
    } else {
      return this.thePath + this.mBaseAlg + '//out//' + this.mBaseAlg + '_algs.json';
    }
  }

  buildDictionary(ut: I_BaseUserAlgs, file: C_ExelData) {

    const names: string[] = [];
    names.push(ut.featuers);
    names.push(ut.categories);

    const dic = new C_Catalog(ut.UT);
    dic.loadFromExel(file, names);
    this.mCatalogs.push(dic);

    if (ut.saveDictionary[0]) {
      // this is for debug purpose only - in case we need to see the 
      // actual 
      const dataAsStr = JSON.stringify(dic);
      const path = this.getOutPath(true);
      this.writeContents(dataAsStr, path);
    }
  }

  writeContents(data, fileName) {
    file.writeFile(fileName, data, function (err) {
      if (err) return console.log(err);
      console.log('Wrote', fileName);
    });
  }

  getIndex(ut: string): number {
    for( let i = 0 ; i < this.mCatalogs.length ; i++) {
      if ( this.mCatalogs[i].mName === ut ) {
        return i;
      }
    }

    console.assert(false);
    return -1;
  }

}
