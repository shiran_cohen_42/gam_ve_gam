export class CLineData {
    tag: string;
    vals: number[] = [];
    constructor(tag: string, vals?: number[]) {
        this.tag = tag;
        if (vals !== undefined) {
            this.vals = vals;
        }
    }

    loadFromLine(line: string[]) {
        console.assert(line.length > 1);
        console.assert(this.tag === line[0]);
        for (let i = 1; i < line.length; i++) {
            const num = parseInt(line[i], 10);
            if (num !== NaN && num !== undefined) {
                this.vals.push(num);
            }
        }
    }

    getVal(index: number) {
        console.assert((index > 0 && index < this.vals.length), 'Cline data request is out of range' + index.toString());
        return this.vals[index];
    }
}
