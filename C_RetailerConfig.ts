import { I_GuiConfig } from './I_GuiConfig';


// tslint:disable-next-line: class-name
export class C_RetailerConfig {
    private mConfig!: I_GuiConfig;

    constructor(config: I_GuiConfig) {
        this.mConfig = config;
    }

    getName(): string {
        console.assert(this.mConfig !== undefined);
        return this.mConfig.retailerName;
    }

    getUserTypes(): string[] {
        console.assert(this.mConfig !== undefined);
        return this.mConfig.userTypes;
    }

    ready(): boolean {
        return (this.mConfig !== undefined);
    }
    // the user of this should be ONLY when creating a CUser
    getRetailer(): I_GuiConfig {
        console.assert(this.mConfig !== undefined);
        return this.mConfig;
    }

    getUti(ut: string): number {
        console.assert(this.mConfig !== undefined);
        let rv = -1;
        this.mConfig.userTypes.forEach((user, i) => {
            if (user === ut) {
                rv = i;
            }
        })

        console.assert(rv > -1);
        return rv;
    }

    loadRetailerStyleImages(userType: string): any[] {
        console.assert(this.mConfig !== undefined);
        const rv: any[] = [];
        const uti = this.getUti(userType);
        const imagesUrlData = this.mConfig.cdnBaseUrl + 'retailer/' +
            + this.mConfig.retailerName + '/styles/';

        const images = this.mConfig.users[uti].imagesForStyles;

        images.forEach((image: string) => {
            const name = image.split('_')[0];
            const val = {
                imageUrl: imagesUrlData + '/' + image,
                featureValue: name,
            };

            rv.push(val);
        });

        return rv;
    }

    feaureCreateUrl(userType: string, feature: string, tag: string): string {
        console.assert(this.mConfig !== undefined);
        const url = this.mConfig.cdnBaseUrl + '/fixed/' + userType + '/' + feature + '/' + tag + '.png';
        return url.split(' ').join('%20');
    }

    bodyCreateUrl(userType: string, feature: string, bodySize: string, tag: string): string {
        console.assert(this.mConfig !== undefined);
        const url = this.mConfig.cdnBaseUrl + '/fixed/' + userType + '/' + feature + '/' + bodySize + '/' + tag + '.png';
        return url.replace(' ', '%20');
    }


}
