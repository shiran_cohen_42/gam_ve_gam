import { CAbsCategory } from './C_AbsCategory';
import { eFeatureType } from './C_AbsFeature';

export class AbsItem {
  // tslint:disable-next-line: variable-name
  _id = '';
  mRetailerName = '';
  mId = '';
  mPage = '';  // path to web page of item
  mTitle = '';
  mDescription = '';
  mCategory!: CAbsCategory;
  mImages: string[] = [];
  mCondition = '';
  mStock = '';
  mAge = '';
  mUT = '';
  mPrice = '';
  mSalePrice = '';
  mRetailer: any;
  mGoogleCategory = ''; // the main <g:google_product_category> field - place holder
  constructor() { };
}

export class CdbItem extends AbsItem {
  fixed: boolean = false;
  constructor(jsonInDb: any) {
    super();
    const obj = JSON.parse(jsonInDb);
    this._id = obj._id;
    this.mId = obj.mId;
    this.mRetailerName = obj.mRetailerName;
    this.mTitle = obj.mTitle;
    this.mDescription = obj.mDescription;
    this.mImages = obj.mImages;
    this.mCondition = obj.mCondition;
    this.mStock = obj.mStock;
    this.mAge = obj.mAge;
    this.mUT = obj.mUT;
    this.mPage = obj.mPage;
    this.mPrice = obj.mPrice;
    this.mSalePrice = obj.mSalePrice;
    console.assert(obj.mCategory !== null);
    const cat = obj.mCategory;
    this.fixed = this.checkCategory( cat);
    // here I allow my self to assume its an item!!
    this.mCategory = new CAbsCategory(cat.name, eFeatureType.eItem, cat.vals);
    console.assert( this.mCategory !== undefined);
    return this;
  }


  // tmp?
  checkCategory( cat: any): boolean {
    const toRemove: number[] = [];

    cat.vals.forEach( (feat: any, i: number) => {
      if ( feat === null ) {
        toRemove.push(i);
      }
    });

    if ( toRemove.length > 0 ) {
      toRemove.forEach( i => {
        cat.vals.splice(i,1);
      });
    } 

    return (toRemove.length > 0);
  }
  categoryName() {
    return this.mCategory.name;
  }

  // need to consolidate Micah!!
  updateCategory(newCat: CAbsCategory) {
    this.mCategory = newCat.copyConstructor()
  }

  belongsTo(retailer: string) {
    return this.mRetailerName === retailer;
  }

  selected(category: string): boolean {
    if (category == 'all') {
      return true;
    } else {
      if (this.mCategory.name === category) {
        return true;
      } else {
        return false;
      }
    }


  }

  basic(retailer: string, ut: string) {
    if (this.mRetailerName !== retailer) {
      return false;
    }
    if (this.mUT !== ut) {
      return false;
    }
    if (this.mStock !== '1') {
      return false;
    }

    return true;
  }

  public forMatch(ut: string, retailer: string) {
    if (this.mRetailerName !== retailer) {
      return false;
    }
    if (this.mUT !== ut) {
      return false;
    }
    if (!this.mCategory.readyForWork()) {
      return false;
    }
    return true;
  }

  setImageToFirts(sel: number) {
    console.assert( sel > 0);
    console.assert( sel < this.mImages.length);
    const tmp = this.mImages[0];
    this.mImages[0] = this.mImages[sel];
    this.mImages[sel] = tmp;
  }
}
