import { CAbsFeature, eFeatureType } from './C_AbsFeature';
import { CAbsCategory } from './C_AbsCategory';
import { CUser } from './CUser';
import { I_GuiConfig } from './I_GuiConfig';
export class C_AbsDictionary {
    mName: string = '';
    mFullDictionary: CAbsCategory[] = [];
    constructor(dicName: string) {
        this.mName = dicName;
    }

    loadFromJson(full: CAbsCategory[]) {
        this.mFullDictionary = full;
    };

    getTheDictionary(): CAbsCategory[] {
        return this.mFullDictionary;
    }

    getUser(): CAbsCategory {
        const user = this.getFullCategory('User');
        console.assert(user.name.length > 0);
        return user;
    }


    getCategoryIndex(name: string): number {
        for (let i = 0; i < this.mFullDictionary.length; i++) {
            if (this.mFullDictionary[i].name === name) {
                return i;
            }
        }
        return -1;
    }


    getFullCategory(req: string): CAbsCategory {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.mFullDictionary.length; i++) {
            if (this.mFullDictionary[i].name === req) {
                return this.mFullDictionary[i];
            }
        }
        console.log('Category: ' + req + ' Not found');
        return new CAbsCategory('null');
    }

    getEmptyCategory(req: string): CAbsCategory {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.mFullDictionary.length; i++) {
            if (this.mFullDictionary[i].name === req) {
                const full = this.mFullDictionary[i];
                const empty = full.getDefaultCategory();
                return empty;
            }
        }
        console.log('Category: ' + req + ' Not found');
        return new CAbsCategory('null');
    }

    getFeatureOptions(featureName: string): CAbsFeature {
        for (let i = 0; i < this.mFullDictionary.length; i++) {
            for (let f = 0; f < this.mFullDictionary[i].vals.length; f++) {
                if (this.mFullDictionary[i].vals[f].name === featureName) {
                    return this.mFullDictionary[i].vals[f].copy();
                }
            }
        }
        console.assert(false, 'Missing feature ' + featureName);
        return new CAbsFeature('mistake', eFeatureType.eSting, [], []);
    }

    getEmptyFeature(featureName: string, belogsToItem: boolean): CAbsFeature {
        const full = this.getFeatureOptions(featureName);
        const empty = full.getEmptyFromFull(belogsToItem);
        return empty;
    }


    validate() {
        this.mFullDictionary.forEach(category => {
            category.vals.forEach(feature => {
                const loF = this.getFeatureOptions(feature.name);
                console.assert(loF !== null, 'Missing feature ' + feature);
            });
        });
    }

    getCategoryListLen() {
        return this.mFullDictionary.length;
    }

    getCategoryList() {
        const arraySt: string[] = [];
        this.mFullDictionary.forEach((c, i) => {
            arraySt.push(c.name);
        });
        return arraySt;
    }

    getCategory(categoryName: string): CAbsCategory {
        // tslint:disable-next-line: prefer-for-of
        for (let c = 0; c < this.mFullDictionary.length; c++) {
            if (this.mFullDictionary[c].name === categoryName) {
                return this.mFullDictionary[c];
            }
        }
        return new CAbsCategory('mistake', eFeatureType.eError, []);
    }

    getTheFeature(fName: string, features: CAbsFeature[]) {
        for (let i = 0; i < features.length; i++) {
            if (features[i].name === fName) {
                return features[i];
            }
        }
        console.assert(false);
    }


    buildDictionary(f: CAbsFeature[], c: CAbsFeature[]) {
        /// build full dictionary
        c.forEach(category => {
            const cat = new CAbsCategory(category.name, category.type, []);
            // full category with all options for menus options
            category.vals.forEach(feature => {
                const fe = this.getTheFeature(feature, f);
                if (fe !== undefined) {
                    cat.vals.push(fe);
                } else {
                    console.assert(false);
                }
            });
            this.mFullDictionary.push(cat);
        });
    }

    getRandTag(feature: string, belogsToItem: boolean) {
        const aFeature = this.getFeatureOptions(feature);
        const len = aFeature.vals.length - 1;
        const randIndex = this.randomRange(0, len);
        const tag = aFeature.vals[randIndex];
        console.assert(tag, 'Set random tag....');
        return tag;
    }

    clearTag(category: CAbsCategory) {
        category.vals.forEach((feature, fi) => {
            feature.vals.length = 0;
        });
    }


    setRandomTags(category: CAbsCategory) {
        category.vals.forEach((feature, fi) => {
            const f = feature.getDefault(category.isItem());
            const numTags = f.getNumOption(category.isItem());
            if (numTags !== feature.vals.length) {
                for (let j = 0; j < numTags; j++) {
                    const tagName = this.getRandTag(feature.name, category.isItem());
                    category.vals[fi].vals[j] = tagName;
                }
            }
        });
    }

    randomRange(min: number, max: number) {
        const range = (max + 1 - min);
        return Math.floor((Math.random() * range));
    }

    getRandomUser(): CUser {
        const fullOptions = this.getFullCategory('User'); // micah has done it again
        const randCategory = fullOptions.generateRandom();
        let tmp = {} as I_GuiConfig;
        tmp.retailerName = 'stam';
        return new CUser(randCategory, fullOptions, tmp);
    }

    getRandomItem(categoryName: string) {
        const fullOptions = this.getFullCategory(categoryName);
        const randCategory = fullOptions.generateRandom();
        return randCategory;
    }


}
