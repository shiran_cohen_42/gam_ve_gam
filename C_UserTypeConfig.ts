import { C_Catalog } from './C_Catalog';
import { CAbsCategory } from './C_AbsCategory';


// tslint:disable-next-line: class-name
export interface I_Category_Item {
    work: CAbsCategory;
    options: CAbsCategory;
}

// tslint:disable-next-line: class-name
export class C_UserTypeConfig {
    private mData: I_Category_Item[] = [];
    private mCatalogs: C_Catalog[] = [];

    constructor(data: C_Catalog[]) {
        data.forEach(catalog => {
            const utcatalog = new C_Catalog(catalog.mName);
            utcatalog.backToAbs(catalog);
            this.mCatalogs.push(utcatalog);
            const ut = this.get(utcatalog, 'User');
            this.mData.push(ut);
        });
        console.assert(this.mData.length > 0);
        console.log('Loader - catalogs');
    }


    private get(from: C_Catalog, category: string): I_Category_Item {
        const op = from.getCategoryOptions(category);
        const wo = op.generateWorkFromOptions();
        return { options: op, work: wo } as I_Category_Item;
    }

    getUT_User(ut: string): I_Category_Item {
        const i = this.ut2i(ut);
        return this.get(this.mCatalogs[i], 'User');
    }

    getUT_Item(userType: string, category: string): I_Category_Item {
        const i = this.ut2i(userType);
        const op = this.mCatalogs[i].getCategoryOptions(category);
        const wo = op.generateWorkFromOptions();
        return { options: op, work: wo } as I_Category_Item;
    }

    getUt(i: number): string {
        console.assert(i < this.mData.length);
        return this.mData[i].options.getUserType();
    }

    ut2i(ut: string): number {
        let rv = -1;
        this.mCatalogs.forEach((c, i) => {
            if (c.mName === ut) {
                rv = i;
            }
        });
        console.assert(rv > -1);
        return rv;
    }

    private valid(ut?: string): void {
        if (ut !== undefined) {
            const i = this.ut2i(ut);
            console.assert(i < this.mData.length);
        }
    }

    getUserType(i: number): string {
        let rv = '';
        this.mData[i].options.vals.forEach(feat => {
            if (feat.name === 'UserType') {
                rv = feat.getCurValue();
            }
        });
        console.assert(rv.length > 0);
        return rv;
    }

    getUserTypes(): string[] {
        this.valid();
        const rv: string[] = [];
        for (let i = 0; i < this.mData.length; i++) {
            const ut = this.getUserType(i);
            rv.push(ut);
        }
        return rv;
    }

    getCategoryList(ut: string): string[] {
        this.valid(ut);
        const rv = this.mCatalogs[this.ut2i(ut)].getCategoryList();
        return rv;
    }

    // the actual value is gotten and saved in the data base, but we need to attach to
    // it the options
    getItemOptions(category: string, ut: string): CAbsCategory {
        const i = this.ut2i(ut);
        const rv = this.mCatalogs[i].getCategory(category);
        return rv;
    }

    getDeault(): string {
        return this.getUserType(0);
    }
}
