import { CAbsFeature, eFeatureType } from './C_AbsFeature';
export class CAbsCategory extends CAbsFeature {
  vals: CAbsFeature[] = [];
  constructor(name: string, type?: eFeatureType, vals?: any[]) {
    super(name, type, vals);
    if (vals !== undefined) {
      vals.forEach(feature => {
        if ( feature !== null ) {
          const newfeature = new CAbsFeature(feature.name, feature.type, feature.vals, feature.maxSelectCount);
          console.assert(newfeature !== null);
          this.vals.push(newfeature);  
        }
      });
    } else {
      this.vals = [];
    }
  }


  isItem(): boolean {
    if (this.type === eFeatureType.eItem) {
      return true;
    } else {
      console.assert(this.type === eFeatureType.eUser);
      return false
    }
  }

  generateWorkFromOptions(): CAbsCategory {
    const work = new CAbsCategory(this.name, this.type, []);
    this.vals.forEach(feature => {

      const empty = feature.getEmptyFromFull(this.isItem());
      work.vals.push(empty)
    });
    return work;
  }

  copyConstructor() {
    const copy = new CAbsCategory(this.name, this.type, this.vals);
    return copy;
  }

  // helper
  toString(): string {
    let str = this.name + ': ';
    this.vals.forEach(tag => {
      str += (tag + ', ');
    });
    return str;
  }

  updateWith(newCategory: CAbsCategory): void {
    this.name = newCategory.name;
    this.type = newCategory.type;
    this.vals = newCategory.vals;
  }

  getDefaultCategory(): CAbsCategory {
    const ret = this.copyConstructor();
    const index = this.isItem() ? 1 : 0;
    ret.vals.forEach(fe => {
      fe.SetDefaults(index);
    });

    return ret;
  }

  getUserType(): string {
    const f = this.getFeature('UserType');
    if (f !== undefined) {
      return f.getCurValue();
    } else {
      console.assert(false);
      return '';
    }
  }

  readyForWork(): boolean {
    let all = 0;
    let withVal = 0;
    this.vals.forEach((f: CAbsFeature) => {
      all++;
      if (f.readyForWork()) {
        withVal++;
      }
    });
    return (all === withVal);
  }

  isEmpty() {
    if (this.vals.length === 0) {
      return true;
    }
    let i;
    for (i = 0; i < this.vals.length; i++) {
      if (this.vals[i].vals.length === 0) {
        return true;
      }
    }
    return false;
  }

  hasThisFeature(feature: string): boolean {
    let rv = true;
    const f = this.getFeature(feature);
    if (f === undefined) {
      rv = false;
    }
    return rv;
  }

  getFeatureVals(feature: string): string[] {
    let rv: string[] = [];
    const f = this.getFeature(feature);
    if (f !== undefined) {
      rv = f.vals;
    }
    return rv;
  }

  getFeature(feature: string): CAbsFeature | undefined {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.vals.length; i++) {
      if (this.vals[i].name === feature) {
        return this.vals[i];
      }
    }
  }

  getFeatureIndex(feature: string): number {
    console.assert(feature.length > 0, 'feature get index - ' + feature);
    console.assert(this.vals.length > 0, 'feature get index len is zero');

    for (let i = 0; i < this.vals.length; i++) {
      if (this.vals[i].name === feature) {
        return (i);
      }
    }
    return -1;
  }


  getVals(feature: string, all: boolean = false) {
    try {
      this.vals.forEach(iCategory => {
        if (iCategory.name === feature) {
          if (all) {
            return iCategory.vals;
          } else {
            return [iCategory.vals[0]];
          }
        }
      });
    } catch {
      const er = new ErrorEvent('failed to get feature:' + feature);
    }
  }

  getVal(feature: string): string {
    try {
      for (let i = 0; i < this.vals.length; i++) {
        if (this.vals[i].name === feature) {
          return this.vals[i].getCurValue();
        }
      }
    } catch {
      const er = new ErrorEvent('failed to get feature:' + feature);
    }
    console.assert(false, 'Did not find a feature');
    return '';
  }

  setCategory(feature: string, val: string[]): boolean {
    for (let i = 0; i < this.vals.length; i++) {
      if (this.vals[i].name === feature) {
        return this.vals[i].set(val, this.isItem());
      }
    }
    return false;
  }

  setValCategory(feature: string, val: string): boolean {
    for (let i = 0; i < this.vals.length; i++) {
      if (this.vals[i].name === feature) {
        this.vals[i].setVal(val, this.isItem());
        return true;
      }
    }

    console.assert(false, 'SetVal: Did not find a feature');
    return false;
  }

  setValsCategory(feature: string, vals: string[]): boolean {
    for (let i = 0; i < this.vals.length; i++) {
      if (this.vals[i].name === feature) {
        this.vals[i].setVals(vals, this.isItem());
        return true;
      }
    }

    console.assert(false, 'SetVals: Did not find a feature');
    return false;
  }

  setSizeIfPossible(): boolean {
    return true;
  }

  generateRandom(): CAbsCategory { // from full options
    const rv = new CAbsCategory(this.name, this.type);

    this.vals.forEach(feature => {
      const randFeatur = feature.generateRandom(this.isItem());
      rv.vals.push(randFeatur);
    })

    return rv;
  }
}
