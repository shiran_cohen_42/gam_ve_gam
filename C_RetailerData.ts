var fs = require('fs');
import { C_System } from './C_System';
import { I_GuiConfig } from './I_GuiConfig';


export interface FirstPass {
    mainSelector: string;
    femaleWords: any[];
    maleWords: any[];
}

export interface Fields {
    LLid: string;
    LLtitle: string;
    LLdescription: string;
    LLimages: string[];
    LLcategory: string[];
    LLcondition: string;
    LLstock: string;
    LLage: string;
    LLgender: string;
    LLpageLink: string;
    LLprice: string;
    LLsalePrice: string;
}

export interface CatMap {
    categoryName: string;
    keyWords: string[];
}

export interface I_ParserData {
    name: string;
    userTypes : string[], //notice duplication with guiConfig.json
    fullUrl: string;
    localUrl: string;
    firstPass: FirstPass;
    fields: Fields;
    maps: CatMap[];
}


const CRetailerInfo = {
    files: ['parser.json', 'guiConfig.json'],
}

export class C_RetailerData {
    mParserData: I_ParserData;
    mGuiData: I_GuiConfig;
    mBasePath: string;

    constructor(name: string) {
        this.mBasePath = new C_System().getRetailerPath(name);
        this.mParserData = this.getParserData(name);
        console.assert(this.mParserData.name === name);
        this.mGuiData = this.getGuiData(name);
        console.assert( this.mGuiData.retailerName === name);
    }

    
    getParser(): I_ParserData {
        console.assert(this.mParserData !== undefined);
        return this.mParserData;
    }

    getGui(): I_GuiConfig {
        console.assert(this.mGuiData !== undefined);
        return this.mGuiData;
    }
  
    private getParserData(name: string): I_ParserData {
        let fullPath = this.mBasePath + CRetailerInfo.files[0];
        console.assert( fs.existsSync(fullPath));

        const jData = this.readFile(fullPath);
        return jData as I_ParserData;
    }

    private getGuiData(name: string): I_GuiConfig {
        let fullPath = this.mBasePath + CRetailerInfo.files[1];
        console.assert( fs.existsSync(fullPath));

        const jData = this.readFile(fullPath);
        return jData as I_GuiConfig;
    }

    private readFile(fullPath: string): any {
        const rawdata = fs.readFileSync(fullPath);
        return JSON.parse(rawdata);
    }
}