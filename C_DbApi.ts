import { AbsItem, CdbItem } from './C_DbItem';



export class C_DbApi {
  mConnected = false;
  mGetDone = false;
  mConnectStr = '';
  mItemList: CdbItem[] = [];
  mFixedItems: CdbItem[] = [];


  inLoading = false;
  items: any;

  constructor(itemsSchema) {
    this.items = itemsSchema;
  }

  // async doneLoading(): Promise<void> {
  //   while (this.inLoading) {
  //     await new Promise(res => setTimeout(res, 100));
  //     console.log('.');
  //   }
  // }

  async doneLoading(): Promise<void> {
    let i = 0;
    while (this.inLoading) {
      if (i % 2) {
        process.stdout.write('-');
      } else {
        process.stdout.write('|');
      }
      await new Promise(res => setTimeout(res, 5));
    }
    // console.log('Items loaded');
  }

  async getAllRetailerItems(retailer: string) {

    this.inLoading = true;
    this.mItemList = [];

    await this.items.find((err: any, dbItems: never[]) => {
      if (err) {
        console.error('Failed to get items ' + retailer);
        return err;
      }
      console.log('Get all of retailers items ' + retailer);

      dbItems.forEach((element: any) => {
        const item = JSON.stringify(element);
        const itDb = new CdbItem(item);
        console.assert(itDb.belongsTo(retailer), 'Must be...');
        this.mItemList.push(itDb);
      });

      console.log(retailer, ' ==> DB load compleated', this.mItemList.length);
      this.inLoading = false;
    }).where('mRetailerName').equals(retailer);

  }

  clearList() {
    this.mItemList = [];
  }

  async getAllItems() {

    this.inLoading = true;
    this.mItemList = [];
    console.log('Get all items ');

    await this.items.find((err: any, dbItems: never[]) => {
      if (err) {
        console.error('Failed to get all items ');
        return err;
      }

      dbItems.forEach((element: any) => {
        const item = JSON.stringify(element);
        const itDb = new CdbItem(item);
        if ( itDb.fixed ) {
          this.mFixedItems.push(itDb); 
        }
        // console.log(itDb.mId);
        this.mItemList.push(itDb);
      });

      console.log(' ==> DB load compleated', this.mItemList.length);
      this.inLoading = false;
    });

  }



  updateItem(item: AbsItem): boolean {
    this.items.findByIdAndUpdate(item._id, item,
      function (err, result) {
        if (err) {
          console.error(err);
          return false;
        }
      });
    return true;
  }

  insertMany(theItems: any): boolean {
    this.items.insertMany(theItems, function (err, dbItems) {
      if (err) {
        console.log(err);
        return false;
      }
    });
    return true;
  }

  async deleteItem( item: CdbItem ) {
    const query = { "_id": item._id };
    await this.items.findByIdAndRemove(query)
    .then( deletedDocument => {
      if(deletedDocument) {
        console.log(`Successfully deleted document that had the form: ${deletedDocument.mId}.`)
      } else {
        console.log("No document matches the provided query.")
      }
      return deletedDocument
    })
    .catch(err => console.error(`Failed to find and delete document: ${err}`))
  }

}
