
export class Workbook {
    name: string;
    data: Array<Array<string>>;
    constructor(name: string, data: Array<Array<string>>) {
        this.name = name;
        this.data = data;
    }
}

export class C_ExelData {
    wbs: Workbook[] = [];
    constructor(from: Workbook[]) {
        from.forEach(wb => {
            this.wbs.push(wb);
        });
    }

    cleanWorkBook(i: number) {
        this.wbs[i].data.forEach((row, ri) => {
            if (row.length > 0) {
                row.forEach((cell, ci) => {
                    switch (typeof (cell)) {
                        case 'string':
                            let t = cell.trim();
                            if (t !== cell) {
                                console.assert(false, this.wbs[i].name, ', ', cell, ' row: ', ri + 1, ' cell: ', ci + 1);
                            }
                            t = cell.replace('  ', ' ');
                            if (t !== cell) {
                                console.assert(false, this.wbs[i].name, ', ', cell, ' row: ', ri + 1, ' cell: ', ci + 1);
                            }
                            t = cell.replace('\'', '');
                            if (t !== cell) {
                                console.assert(false, this.wbs[i].name, ', ', cell, ' row: ', ri + 1, ' cell: ', ci + 1);
                            }
                            break;
                        case 'number':
                            console.assert(cell === 0 || cell === 1);
                            break;
                        default:
                            console.assert(false, this.wbs[i].name, 'row: ', ri, ' cell: ', ci);
                    }


                    try {
                    } catch {
                        console.assert(false);
                    }

                });
            }
        });
    }

    getWorkBook(name: string): Workbook {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.wbs.length; i++) {
            if (this.wbs[i].name === name) {
                this.cleanWorkBook(i);
                return this.wbs[i];
            }
        }
        console.assert(false);
        return this.wbs[0];
    }

}

