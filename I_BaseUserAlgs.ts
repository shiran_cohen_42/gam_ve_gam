export class I_BaseUserAlgs {
  exelFileName: string;
  UT: string;
  featuers: string;
  categories: string;
  saveDictionary: boolean[];
  algs: string[][];
  constructor(obj: any) {
    this.exelFileName = obj.exelFileName;
    this.UT = obj.UT;
    this.categories = obj.categories;
    this.featuers = obj.featuers;
    this.saveDictionary = obj.saveDictionary;
    this.algs = obj.algs;
  }
}
