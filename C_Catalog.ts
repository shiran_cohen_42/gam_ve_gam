import { C_ExelData } from "./C_ExelData";
import { CAbsFeature } from './C_AbsFeature';
import { C_AbsDictionary } from './C_AbsDictionary';
import { CAbsCategory } from './C_AbsCategory';

export class C_Catalog extends C_AbsDictionary {
  constructor(name: string) {
    super(name)
  }


  loadFromExel(aFile: C_ExelData, vals: string[]) {
    const f: CAbsFeature[] = [];
    const c: CAbsFeature[] = [];

    let wb = aFile.getWorkBook(vals[0]);
    for (let i = 1; i < wb.data.length; i++) {
      if (wb.data[i].length > 0) {
        const curLine = wb.data[i];
        const cf = new CAbsFeature(curLine[0]);
        cf.fromLine(curLine);
        f.push(cf);
      }
    }

    wb = aFile.getWorkBook(vals[1]);
    for (let i = 1; i < wb.data.length; i++) {
      if (wb.data[i].length > 0) {
        const curLine = wb.data[i];
        const cf = new CAbsFeature(curLine[0]);
        cf.fromLine(wb.data[i]);
        c.push(cf);
      }
    }

    this.buildDictionary(f, c);

  }

  backToAbs_bad(data: any) {
    this.mFullDictionary = data.mFullDictionary;
    console.assert(this.mName.length > 0);
  }

  backToAbs(data: any) {
    const f: CAbsFeature[] = [];
    const c: CAbsFeature[] = [];

    data.mFullDictionary.forEach((cat: any) => {
      const newCat = new CAbsCategory(cat.name, cat.type, cat.vals);
      this.mFullDictionary.push(newCat)
      if (!newCat.isItem()) {

        const tmp = newCat.getFeature('UserType');
        if (tmp !== undefined) {
          this.mName = tmp.getCurValue();
        } else {
          console.assert(false, 'UserType must be present');
        }

      }
    });

    console.assert(this.mName.length > 0);
  }

  // for GUI - the options - tagging
  getCategoryOptions(categoryName: string): CAbsCategory {
    let rv!: CAbsCategory;
    this.mFullDictionary.forEach(cat => {
      if (cat.name === categoryName) {
        rv = cat;
      }
    });
    console.assert(rv.vals.length > 0);
    return rv;
  }

  getUserType(): string {
    let rv = '';

    const cat = this.getCategory('User');
    const fea = cat.getFeature('UserType');
    if (fea !== undefined) {
      return fea.getCurValue();
    } else {
      console.assert(false);
    }
    return rv;
  }

  // A user is specific to retailer - the creation of user is not here
  // Catolog are independent of retailer
  getAuser(): CAbsFeature[] {
    const features: CAbsFeature[] = [];
    this.mFullDictionary.forEach(cat => {
      if (cat.name === 'User') {
        cat.vals.forEach((feature: CAbsFeature) => {
          const e = feature.getEmptyFromFull(false);
          features.push(e);
        })
      }
    });
    console.assert(features.length > 0);
    return features;
  }

  getUserOptions(): CAbsCategory {
    let ut = new CAbsCategory('stam');
    this.mFullDictionary.forEach(cat => {
      if (cat.name === 'User') {
        ut = new CAbsCategory(cat.name, cat.type, cat.vals);
      }
    });
    console.assert(ut.vals.length > 0);
    return ut;
  }



  validateCat(catName: string): boolean {
    let rv = false;
    this.mFullDictionary.forEach(cat => {
      if (cat.name === catName) {
        rv = true;
      }
    });
    return rv;
  }

  getCategoryIndex(catName: string): number {
    let rv = -1;
    this.mFullDictionary.forEach((cat, i) => {
      if (cat.name === catName) {
        rv = i;
      }
    });
    return rv;
  }

}


