
export class CItemResult {
  bMatch!: boolean;
  category!: string;
  image!: string;
  page!: string;
  id!: string;
  title!: string;
  price!: string;
  salePrice!: string;
  story!: string;

  constructor(obj?: any) {
    if (obj === undefined) { return; }
    this.bMatch = obj.bMatch;
    this.story = obj.story;
    this.category = obj.category;
    this.image = obj.image;
    this.page = obj.page;
    this.title = obj.title;
    this.id = obj.id;
    this.price = obj.price;
    this.salePrice = obj.salePrice;
  }

  fromMatchResut(match: boolean, cat: string, image: string,
    page: string, id: string, title: string, price: string, salePrice: string, story: string) {
    this.bMatch = match;
    this.category = cat;
    this.image = image;
    this.page = page;
    this.id = id;
    this.story = story;
    this.title = title;
    this.price = price;
    this.salePrice = salePrice;
  }
}

export class CCategoryResultList {
  categoryName = '';
  matchList: CItemResult[] = [];
  rejectList: CItemResult[] = [];
  private mDebug = false;
  mResInPage = 0;

  constructor(category: string, debug = false) {
    this.mDebug = debug;
    this.categoryName = category;
  }

  category(): string {
    return this.categoryName;
  }

  add(res: CItemResult): void {
    if (this.mDebug) {
      if (res.bMatch) {
        this.matchList.push(res);
      } else {
        this.rejectList.push(res);
      }
    } else {
      if (res.bMatch) {
        this.matchList.push(res);
      }
    }
  }

}

export class CAskLilyResults {
  mList: CCategoryResultList[] = [];
  private mDebug: boolean;
  constructor(debug = false) {
    this.mDebug = debug;
  }

  countCategories(): number[] {
    const rv: number[] = [];
    
    this.mList.forEach(cat => {
      rv.push(cat.matchList.length);
    });
    return rv;
  }

  count(match?: boolean): number {
    let len = 0;
    let countMatch = false;
    let counReject = false;

    if (match === undefined) {
      counReject = countMatch = true;
    } else {
      if (match) {
        countMatch = true;
      } else {
        counReject = true
      }
    }

    this.mList.forEach(cat => {
      if (countMatch) {
        len += cat.matchList.length;
      }
      if (counReject) {
        len += cat.rejectList.length;
      }
    });
    return len;
  }

  getIndex(name: string): number {
    let res = -1;
    for (let i = 0; i < this.mList.length; i++) {
      if (this.mList[i].category() === name) {
        return i;
      }
    }
    console.assert(res === -1);
    return this.mList.push(new CCategoryResultList(name, this.mDebug)) - 1;
  }

  add(res: CItemResult) {
    const i = this.getIndex(res.category);
    this.mList[i].add(res);
  }

  parseResults(res: any, debug: boolean): number {
    let count = 0;
    if (res === null || res === undefined || res.length === 0) {
      console.log('Got empty results');
      return count;
    }

    res.mList.forEach((category: any) => {
      category.matchList.forEach((res: any) => {
        const r = new CItemResult(res);
        this.add(r);
        count++;
      });

      category.rejectList.forEach((res: any) => {
        const r = new CItemResult(res);
        this.add(r);
        count++;
      });
    });;

    return count;
  }

  randomRange(min: number, max: number): number {
    const range = (max  - min);
    if ( range === 0 ) {
      return range;
    }

    const rv =  Math.floor((Math.random() * range));
    console.assert( rv < max);
    return rv;
  }
  
  randCat( len: number, categories: number[]) {
    const rand = this.randomRange(0, len);
    let count = categories[0];
    for ( let i = 0 ; i < categories.length ; i++ ) {
      if ( rand < count ) {
        return i;
      } else {
        if ( i+1 === categories.length ) {
          return i;
        } else {
          count += categories[i+1];
        }
      }
    } 
    // console.assert(false);
    return 0;
  }

  shuffle(arrayLen: number) {
    var currentIndex = arrayLen, temporaryValue, randomIndex;
    const array: number[] = [];
    for ( let i = 0 ; i < arrayLen ; i++) {
      array.push(i);
    } 
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

  getRandomIndexes() {
    // each category will get a randomize index
    const randIndex: number[][] = [];

    for ( let c = 0 ; c < this.mList.length ; c++ ) {
      const len = this.mList[c].matchList.length;
      randIndex.push(this.shuffle(len));
    }

    return randIndex;
  }

  getRandomDisplayList(maxRows?: number): CItemResult[][] {
    
    const randList = this.getRandomIndexes();
    const count = this.count(true);

    let max = count / 2;
    if ( maxRows !== undefined  && maxRows < max) {
      max = maxRows;
    }

    const categories = this.countCategories();

    const res: CItemResult[][] = [];
    let tmp: CItemResult[] = [];
    const indexs: number[] = [];

    for ( let c = 0 ; c < this.mList.length ; c++ ) {
      indexs.push(0);
    }

    
    for ( let c = 0 ; c < max ; ) {
      const cat = this.randCat(count, categories);
      // const cat = this.randomRange(0, this.mList.length);
      const i = indexs[cat]++;
      // console.assert( i < this.mList.length);
      const iInRand = randList[cat][i];

      if ( iInRand < this.mList[cat].matchList.length) {
  
        console.assert(iInRand !== NaN);
        const item = this.mList[cat].matchList[iInRand];
  
        tmp.push(item);
        
        if ( c % 2 === 1) {
          res.push(tmp);
          tmp = [];
        }
        c++;
      }      
      
    }

    return res;
  }
}

